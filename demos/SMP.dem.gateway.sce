// Copyright (C) 2010 - 2011 - Allan CORNET
//

function subdemolist = demo_SMP_gw()
  demopath = get_absolute_file_path("SMP.dem.gateway.sce");
  subdemolist = ["demo SMP"             ,"SMP.dem.sce"];
  subdemolist(:,2) = demopath + subdemolist(:,2);
endfunction

subdemolist = demo_SMP_gw();
clear demo_SMP_gw;


