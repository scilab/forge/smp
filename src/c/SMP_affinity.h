//------------------------------------------------------------------------------
// Allan CORNET - 2010
//------------------------------------------------------------------------------
#ifndef __SMP_PRIORITY_H__
#define __SMP_PRIORITY_H__

int *SMP_getScilabCpuAffinity(int *sizeArrayReturned);

int SMP_setScilabCpuAffinity(const int *affinityArray, int sizeAffinityArray);

int SMP_getNumberOfCpus(void);

#endif /* __SMP_PRIORITY_H__ */
//------------------------------------------------------------------------------
