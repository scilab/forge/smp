//------------------------------------------------------------------------------
// Allan CORNET - 2010
//------------------------------------------------------------------------------
#include <windows.h>
#include "SMP_priority.h"
//------------------------------------------------------------------------------
static int SMP_convertPrioryForUser(int priority);
static int SMP_convertPrioryForSystem(int priority);
//------------------------------------------------------------------------------
int SMP_getScilabPriority(void)
{
     DWORD dwPriClass = GetPriorityClass(GetCurrentProcess());
     return (int) SMP_convertPrioryForUser((int)dwPriClass);
}
//------------------------------------------------------------------------------
int SMP_setScilabPriority(int newPriority, int *iErr)
{
    *iErr = 0;
    if (!SetPriorityClass(GetCurrentProcess(), (DWORD)SMP_convertPrioryForSystem(newPriority)))
    {
        *iErr = (int)GetLastError();
    }
    return SMP_getScilabPriority();
}
//------------------------------------------------------------------------------
int SMP_isValidPriority(int newPriority)
{
    return (SMP_convertPrioryForSystem(newPriority) != 0);
}
//------------------------------------------------------------------------------
static int SMP_convertPrioryForUser(int priority)
{
    switch (priority)
    {
    case IDLE_PRIORITY_CLASS: return 4;
    case BELOW_NORMAL_PRIORITY_CLASS: return 6;
    case NORMAL_PRIORITY_CLASS: return 8;
    case ABOVE_NORMAL_PRIORITY_CLASS: return 10;
    case HIGH_PRIORITY_CLASS: return 13;
    case REALTIME_PRIORITY_CLASS: return 24;
    }
    return 0;
}
//------------------------------------------------------------------------------
static int SMP_convertPrioryForSystem(int priority)
{
    switch (priority)
    {
    case  4: return IDLE_PRIORITY_CLASS;
    case  6: return BELOW_NORMAL_PRIORITY_CLASS;
    case  8: return NORMAL_PRIORITY_CLASS;
    case 10: return ABOVE_NORMAL_PRIORITY_CLASS;
    case 13: return HIGH_PRIORITY_CLASS;
    case 24: return REALTIME_PRIORITY_CLASS;
    }
    return 0;
}
//------------------------------------------------------------------------------
