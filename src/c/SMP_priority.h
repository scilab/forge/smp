//------------------------------------------------------------------------------
// Allan CORNET - 2010
//------------------------------------------------------------------------------
#ifndef __SMP_PRIORITY_H__
#define __SMP_PRIORITY_H__

int SMP_getScilabPriority(void);

int SMP_setScilabPriority(int newPriority, int *iErr);

int SMP_isValidPriority(int newPriority);

#endif /* __SMP_PRIORITY_H__ */
//------------------------------------------------------------------------------
