/* ==================================================================== */
/* Allan CORNET - 2010 */
/* ==================================================================== */
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "SMP_priority.h"
/* ==================================================================== */
static int sci_SMP_setpriority(char *fname);
static int sci_SMP_getpriority(char *fname);
/* ==================================================================== */
int sci_SMP_priority(char *fname)
{
    Rhs = Max(0, Rhs);
    CheckRhs(0, 1);
    CheckLhs(1, 1);

    if (Rhs == 0)
    {
        return sci_SMP_getpriority(fname);
    }
    else
    {
        return sci_SMP_setpriority(fname);
    }
    return 0;
}
/* ==================================================================== */
static int sci_SMP_setpriority(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isDoubleType(pvApiCtx, piAddressVarOne))
    {
        double dValue = 0.;
        int iValue = 0;

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 1);
            return 0;
        }

        getScalarDouble(pvApiCtx, piAddressVarOne, &dValue);
        iValue = (int)dValue;

        if ((double)iValue != dValue)
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: A integer value expected.\n"), fname, 1);
            return 0;
        }

        if (SMP_isValidPriority(iValue))
        {
            int iErr = 0;
            SMP_setScilabPriority(iValue, &iErr);
            if (iErr != 0)
            {
                Scierror(999,_("%s: System Error %d.\n"), fname, iErr);
            }
            return sci_SMP_getpriority(fname);
        }
        else
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: A valid value expected.\n"), fname, 1);
            return 0;
        }
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: An integer value expected.\n"), fname, 1);
        return 0;
    }
}
/* ==================================================================== */
static int sci_SMP_getpriority(char *fname)
{
    createScalarDouble(pvApiCtx, Rhs + 1, (double)SMP_getScilabPriority());
    LhsVar(1) = Rhs + 1;
    C2F(putlhsvar)();
    return 0;
}
/* ==================================================================== */
